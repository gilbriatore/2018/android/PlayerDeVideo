package br.edu.up.playerMP4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    String prefixo = "android.resource://";
    String path = prefixo +  getPackageName() + "/" + R.raw.big_buck_bunny;

    VideoView videoView = (VideoView) findViewById(R.id.videoView);
    videoView.setMediaController(new MediaController(this));
    videoView.setVideoPath(path);
    videoView.start();
  }
}
